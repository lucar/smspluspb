/*
 *  Copyright (c) 2012 masck84 free for not commercial use
*/

#ifdef PLAYBOOK

#include <bps/bps.h>
#include <bps/dialog.h>
#include <dirent.h>
#include "list.h"

void createRomDirListing( const char *dpath );
int openLoadRomDialogAndStartRom( const char *dpath );

#endif
