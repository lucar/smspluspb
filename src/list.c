/*
 *  Copyright (c) 2012 masck84 free for not commercial use
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"

llist_t *__list = 0;
int __list_size = 0;
int __list_created = 0;
char **__toarray_list = 0;

char** convertListToArray()
{
	if(__toarray_list)
	{
		free(__toarray_list);
		__toarray_list = 0;
	}

	__toarray_list = (char**) malloc(sizeof(char*)*__list_size);

	llist_t *current = getCurrentList();
	int i = 0;
	for(; i< __list_size; ++i, current = current->next)
		__toarray_list[i] = current->filename;

	return __toarray_list;
}

int getListSize()
{
	return __list_size;
}

void createList()
{
	freeList();
	//__list = (llist_t*) malloc(sizeof(llist_t));
	__list_created = 1;
	__list_size = 0;
}

llist_t* getCurrentList()
{
	if(!__list_created)
		return 0;

	return __list;
}

void freeList()
{
	llist_t *current = getCurrentList();
	llist_t *head = getCurrentList();
	while(head)
	{
		free(current);
		head = current->next;
	}

	if(__toarray_list)
	{
		free(__toarray_list);
		__toarray_list = 0;
	}

	__list = 0;
	__list_created = 0;
	__list_size = 0;
}

void removeItem(char* filename)
{
	if(!__list_created || !__list_size)
		return;

	__removeItem(&__list, filename);
}
void addItem(char* filename)
{
	if(!__list_created)
		return;

	__addItem(&__list, filename);
}

llist_t* __createItem(char* filename)
{
	llist_t *anitem = (llist_t*) malloc(sizeof(llist_t));
	strcpy(anitem->filename,filename);
	anitem->next = 0;

	return anitem;
}

void __removeItem(llist_t **head, char* filename)
{
	llist_t *current = *head;
	llist_t *prev = 0;
	for(; current != 0; prev = current,current = current->next)
	{
		if(!strcmp(current->filename,filename))
		{
			if(prev)
			{
				prev->next = current->next;
			}
			else
				*head = 0;

			free(current);
			--__list_size;
			break;
		}
	}
}

void __addItem(llist_t **head, char* filename)
{
	llist_t *current = *head;
	llist_t *item = __createItem(filename);
	if(!current)
		*head = item;
	else
	{
		item->next = current->next;
		current->next = item;
	}
	++__list_size;
}

