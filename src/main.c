
/*
    Copyright (c) 2002, 2003 Gregory Montoir

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <SDL/SDL.h>
#include "sdlsms.h"
#include "filters.h"
#include "saves.h"
#include "shared.h"
#include "common.h"

static t_config cfg;
static t_named_filter filters[] = {
  {"2xsai",      FILTER_2XSAI},
  {"super2xsai", FILTER_SUPER2XSAI},
  {"supereagle", FILTER_SUPEREAGLE},
  {"advmame2x",  FILTER_ADVMAME2X},
  {"tv2x",       FILTER_TV2X},
  {"2x",         FILTER_NORMAL2X},
  {"bilinear",   FILTER_BILINEAR},
  {"dotmatrix",  FILTER_DOTMATRIX}
};

static int parse_args(int argc, char **argv)
{
  int i;

  /* default virtual console emulation settings */
  sms.use_fm = 0;
  sms.country = TYPE_OVERSEAS;

  cfg.filter = -1;

#if !defined PLAYBOOK
  strcpy(cfg.game_name, argv[1]);
#endif

  for(i = 2; i < argc; ++i) {
    if(strcasecmp(argv[i], "--fm") == 0)
      sms.use_fm = 1;
    else if(strcasecmp(argv[i], "--japan") == 0)
      sms.country = TYPE_DOMESTIC;
    else if(strcasecmp(argv[i], "--usesram") == 0) {
      cfg.usesram = 1;
      sms.save = 1;
    }
    else if(strcasecmp(argv[i], "--fskip") == 0) {
      if(++i<argc) {
        cfg.frameskip = atoi(argv[i]);
      }
    }
    else if(strcasecmp(argv[i], "--fullspeed") == 0)
      cfg.fullspeed = 1;
    else if(strcasecmp(argv[i], "--fullscreen") == 0)
      cfg.fullscreen = 1;
    else if(strcasecmp(argv[i], "--nosound") == 0)
      cfg.nosound = 1;
    else if(strcasecmp(argv[i], "--joystick") == 0)
      cfg.joystick = 1;
    else if(strcasecmp(argv[i], "--filter") == 0) {
      i++;
      if(i < argc) {
        int j = 0;
        for( ; j < sizeof(filters) / sizeof(filters[0]); ++j) {
          if(strcasecmp(filters[j].name, argv[i]) == 0) {
            cfg.filter = filters[j].type;
            break;
          }
        }
      }
    }
    else 
      printf("WARNING: unknown option '%s'.\n", argv[i]);
  }
  return 1;
}

int main(int argc, char **argv)
{
  printf("%s (Build date %s)\n", SMSSDL_TITLE, __DATE__);
  printf("(C) Charles Mac Donald in 1998, 1999, 2000\n");
  printf("SDL Version by Gregory Montoir (cyx@frenchkiss.net)\n");
  printf("\n");

#if defined PLAYBOOK
  if(argc < 1) {
#else
  if(argc < 2) {
#endif
    int i;
    printf("Usage: %s <filename.<SMS|GG>> [--options]\n", argv[0]);
    printf("Options:\n");
    printf(" --fm           \t enable YM2413 sound.\n");
    printf(" --japan        \t set the machine type as DOMESTIC instead of OVERSEAS.\n");
    printf(" --usesram      \t load/save SRAM contents before starting/exiting.\n");
    printf(" --fskip <n>    \t specify the number of frames to skip.\n");
    printf(" --fullspeed    \t do not limit to 60 frames per second.\n");
    printf(" --fullscreen   \t start in fullscreen mode.\n");
    printf(" --joystick     \t use joystick.\n");
    printf(" --nosound      \t disable sound.\n");
    printf(" --filter <mode>\t render using a filter: ");
    for(i = 0; i < sizeof(filters) / sizeof(filters[0]) - 1; ++i)
      printf("%s,", filters[i].name);
    printf("%s.", filters[i].name);
    return 1;
  }

  memset(&cfg, 0, sizeof(cfg));
  if(!parse_args(argc, argv))
	  return 0;

#ifdef PLAYBOOK

#if defined(PLAYBOOK_SIM_DEBUG)
  const char *dpath = "app/native/assets/";
#else
  const char *dpath = "/accounts/1000/shared/misc/roms/sms/";
#endif
	bps_initialize();
	dialog_request_events(0);    //0 indicates that all events are requested
	createRomDirListing(dpath);
	openLoadRomDialogAndStartRom(dpath);
#endif

	if(sdlsms_init(&cfg))
	{
		sdlsms_emulate();
		sdlsms_shutdown();
	}

#ifdef PLAYBOOK
  freeList();
#endif

  return 0;
}

#ifdef PLAYBOOK
void createRomDirListing( const char *dpath )
{
	DIR* dirp;
	struct dirent* direntp;

	if(!dpath)
	{
	  fprintf(stderr,"dpath is null.\n");
	  return;
	}
	else
		fprintf(stderr,"reading dir is %s.\n",dpath);


	createList();

	dirp = opendir( dpath);
	if( dirp != NULL )
	{
		 for(;;)
		 {
			direntp = readdir( dirp );
			if( direntp == NULL )
			  break;

			fprintf(stderr,"FILE: '%s' \n", direntp->d_name);

			if( strcmp( direntp->d_name, ".") == 0 || strcmp( direntp->d_name,"..") == 0)
			  continue;

			if( strstr(direntp->d_name, "sms") ||
					strstr(direntp->d_name, "SMS") ||
					strstr(direntp->d_name, "GG") ||
					strstr(direntp->d_name, "gg")  )
			{
			 // fprintf(stderr,"ROM: %s\n", direntp->d_name);
				addItem(direntp->d_name);
			}
		 }
	}
	else
	{
		fprintf(stderr,"dirp is NULL ...\n");
	}

}

int openLoadRomDialogAndStartRom(const char* dpath)
{
	static char dirpath[256];
	char *currpath = dirpath;

	if(dpath)
		strcpy(dirpath,dpath);

	// static int gameIndex;
	int status = 0;
	int count = 0;

	// ROM selector
	dialog_instance_t dialog = 0;
	int i, rc;
	bps_event_t *event;
	int domain = 0;
	const char * label;

	dialog_create_popuplist(&dialog);
	const char** list = convertListToArray();
	dialog_set_popuplist_items(dialog, list, getListSize());

	char* cancel_button_context = "Canceled";
	char* okay_button_context = "Okay";
	dialog_add_button(dialog, DIALOG_CANCEL_LABEL, true, cancel_button_context, true);
	dialog_add_button(dialog, DIALOG_OK_LABEL, true, okay_button_context, true);
	dialog_set_popuplist_multiselect(dialog, false);
	dialog_show(dialog);

	while(1)
	{
		 bps_get_event(&event, -1);

		 if (event)
		 {
			 domain = bps_event_get_domain(event);
			 if (domain == dialog_get_domain())
			 {
				 int *response[1];
				 int num;

				 label = dialog_event_get_selected_label(event);

				 if(strcmp(label, DIALOG_OK_LABEL) == 0)
				 {
					 dialog_event_get_popuplist_selected_indices(event, (int**)response, &num);
					 if(num != 0)
					 {
						 //*response[0] is the index that was selected
						 printf("%s", list[*response[0]]);fflush(stdout);
						 strcpy(cfg.game_name, currpath);
						 //strcat(cfg.game_name, "/");
						 strcat(cfg.game_name, list[*response[0]]);
						 printf("full rom path %s\n",cfg.game_name);

						 if(!dpath)
						 {
							printf("reinit system\n");
							system_shutdown();
							if(load_rom(cfg.game_name) == 0)
							{
								printf("ERROR: can't load `%s'.\n", cfg.game_name);
								return 0;
							}
							else
								printf("Loaded `%s'.\n", cfg.game_name);
							system_init(cfg.nosound ? 0 : SOUND_FREQUENCY);
						 }
					 }
					 bps_free(response[0]);
				 }
				 else
				 {
					 printf("User has canceled ISO dialog.");
					 return 0;
				 }
				 break;
			 }
		 }
	}

 return 1;
}
#endif
