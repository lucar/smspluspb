/*
 *  Copyright (c) 2012 masck84 free for not commercial use
*/

#define MAX_FILENAME_SIZE 512

typedef struct llist_
{
    char filename[512];
    struct llist_ *next;
}llist_t;

void createList();
void freeList();
llist_t* getCurrentList();
int getListSize();
void addItem(char* filename);
void removeItem(char* filename);
char** convertListToArray();


/* private */
static void __addItem(llist_t **l, char* filename);
static llist_t* __createItem(char* filename);
static void __removeItem(llist_t **l, char* filename);
